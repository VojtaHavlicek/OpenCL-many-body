__kernel void integrate_eom(
        __global float4* oldPosition,
        __global float4* oldVelocity,
        __global float4* newPosition,
        __global float4* newVelocity,
        __local float4* localPos,
        int numElectrons,
        float deltaTime)
{
    unsigned int tid = get_local_id(0);
    unsigned int gid = get_global_id(0);
    unsigned int localSize = get_local_size(0);
    
    unsigned int numTilesElectrons = numElectrons/localSize;
    float4 pos = oldPosition[gid];
    float4 vel = oldVelocity[gid];

    float m = 1.0;

    // acceleration vectors:
    float2 acc     = (float2)(0.0,0.0);
    float2 acc_aux = (float2)(0.0,0.0);

    for(int i = 0; i < numTilesElectrons; ++i)
    {
        int idx = i * localSize + tid;
        localPos[tid] = oldPosition[idx];

        barrier(CLK_LOCAL_MEM_FENCE);

        // MUTUAL ACCELERATION? 
        acc_aux.x = 0.0;
        acc_aux.y = 0.0;

        for(int j = 0; j < localSize; ++j)
        {
            float4 r = localPos[j] - pos;
            float distSqr = r.x*r.x + r.y*r.y;

            distSqr += 1e-9; // Why?

            // Pauli force? Damn :D
            float s= m*m*localPos[j].w*pos.w/distSqr;
            { // ?
                acc_aux.x += -s*r.x;
                acc_aux.y += -s*r.y;
            }
        }

        acc.x += acc_aux.x;
        acc.y += acc_aux.y;
        barrier(CLK_LOCAL_MEM_FENCE);
    }

    // POSITIVE BACKGROUND CCGS
    {
        acc.x += -0.5*m*pos.x;
        acc.y += -0.5*m*pos.y;
    }

    // EULER
    {
        vel.x += acc.x * deltaTime;
        vel.y += acc.y * deltaTime;

        pos.x += vel.x * deltaTime;
        pos.y += vel.y * deltaTime;
    }

    newPosition[gid] = pos;
    newVelocity[gid] = vel;
}
