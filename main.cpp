// MANY BODY COLLISION OPENCL IMPLEMENTATION TEST
// inspired by NVidia GPU Gems & T. Kramer's 
// many-body collision code implementation on  GPU
//
// This is a mock Newton-interaction euler integrated
// test. 
//
// Vojta Havlicek 2013

#define __NO_STD_VECTOR
#define __CL_ENABLE_EXCEPTIONS
#define __USE_MATH_DEFINES

#include <boost/chrono.hpp>
#include <boost/thread.hpp>

#include <GL/glew.h>
#include <GL/glut.h>

#include <CL/opencl.h>
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <cmath>
#include <ctime>

// CL VARS:
const size_t PARTICLES = 256; 
const size_t GROUPSIZE = 256;
const size_t MEMSIZE = PARTICLES*4*sizeof(float);
float dt = 1e-3; // timestep

cl_platform_id   platform_id;
cl_device_id     device_id;
cl_context       context;
cl_command_queue command_queue;
cl_program       program;
cl_kernel        kernel;
cl_int           ret;
cl_uint          num_platforms;
cl_uint          num_devices;

// MEMORY OBJECTS. EACH ONE HAS SIZE
// 4*PARTICLES*sizeof(float) DUE TO
// 4 - tuple optimisation of GPU

cl_mem gposold; // old positions
cl_mem gposnew; // new positions
cl_mem gvelnew; // new velocities
cl_mem gvelold; // old velocities

float* posold; // old positions
float* velold; // old velocities
float* posnew; // new positions
float* velnew; // new velocities

// FUNCTION DECLARATIONS
void cl_prepare();
void allocate_memory();
void cpu_initialize();
void cl_attach_kernel();
void cpu_propagate();
void write_state(const char* filename);
void put_electrons(int num, float* pos, float* vel);
void get_electrons(int num, float* pos, float *vel);
std::string loadFile(const char * filename);

// -------------------------------------------
// GL VARS
boost::chrono::steady_clock::time_point timer;

double accumulator;
 float t;
 float dtt;
 int width;
 int height;
 int window_handle;
 int counter;

 void update();
 void tick();
 void render();

// ---------------------------------------------
// DEFINITIONS
// Main 
int main(int argc, char *argv[])
{
    // OPEN-GL for rendering
    timer = boost::chrono::steady_clock::now();

    width = 600;
    height = 600;

    t = 0;
    dtt = 1.0f/30.0f;

    glutInit(&argc, argv);
    glutInitWindowPosition(100,100);
    glutInitWindowSize(width, height);
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
    window_handle = glutCreateWindow("Plasma visualisation in CL/GL");
    glutDisplayFunc(update);
    glutIdleFunc(update);

    glewInit();

    // OPEN-CL for computation
    cl_prepare(); 
    cpu_initialize();
    allocate_memory();
    cl_attach_kernel();
    put_electrons(PARTICLES, posold, velold);
    
    glutMainLoop();


    //for( int i = 0; i < 1000; i++)
    //{
    //    if(i % 100 == 0){
    //        char filename[200];
    //        sprintf(filename, "data/gpu%05d.dat", int(i/100));
    //        write_state(filename);
    //    }
    //    cpu_propagate();
    //} 
    std::cout << "No segfault, no fun \n";


    return 0;
}


void update()
{
    boost::chrono::steady_clock::time_point current = boost::chrono::steady_clock::now();
    double diff = (boost::chrono::duration<double> (current - timer)).count();

    if(diff > 0.1)
        diff = 0.1;

    timer = current;

    accumulator += diff;

    while(accumulator >= dt)
    {
        accumulator -= dt;
        t += dt;
        tick();
    }
    render();
    cpu_propagate(); 
}

void tick()
{
    get_electrons(PARTICLES, posold, velold);
}

// This will take some time :D
void render()
{
    float x_, y_, dw_;

    dw_ = 1/(200.0f);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glBegin(GL_QUADS);

    for (int i = 0; i < PARTICLES; i++) 
    {
        x_ = 8.0f*(posold[i*4]    /(PARTICLES));
        y_ = 8.0f*(posold[i*4 + 1]/(PARTICLES));
        glColor3f(1.0f,1.0f,1.0f);

        glVertex2f(x_-dw_, y_-dw_);
        glVertex2f(x_+dw_, y_-dw_);
        glVertex2f(x_+dw_, y_+dw_);
        glVertex2f(x_-dw_, y_+dw_);
    }

    glEnd();

    glutSwapBuffers();
}




// OPENCL STUFF THAT WORKS IS BELOW (I AM BEING A BIT DIRTY HERE AND I KNOW IT :D)
//
// PREPARES A SINGLE DEVICE GPU OPENCL CONTEXT
void cl_prepare()
{
    clGetPlatformIDs(1, &platform_id, &num_platforms);
    clGetDeviceIDs(platform_id, CL_DEVICE_TYPE_GPU, 1, &device_id, &num_devices);
    context       = clCreateContext(NULL, 1, &device_id, NULL, NULL, &ret);
    command_queue = clCreateCommandQueue(context, device_id, 0, &ret);
}

// PROPAGATES and performs computation of the system 
void cpu_propagate()
{
    clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)& gposold);
    clSetKernelArg(kernel, 1, sizeof(cl_mem), (void *)& gvelold);
    clSetKernelArg(kernel, 2, sizeof(cl_mem), (void *)& gposnew);
    clSetKernelArg(kernel, 3, sizeof(cl_mem), (void *)& gvelnew);
    clSetKernelArg(kernel, 4, sizeof(cl_float)*GROUPSIZE, NULL); // Critical
    clSetKernelArg(kernel, 5, sizeof(int)   , (void *) &PARTICLES);
    clSetKernelArg(kernel, 6, sizeof(float) , (void *) &dt);

    cl_event eventA;
    ret = clEnqueueNDRangeKernel(command_queue, kernel, 1, NULL, &PARTICLES, &GROUPSIZE, 0, NULL, &eventA );
    clWaitForEvents(1, &eventA);

    clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)& gposnew);
    clSetKernelArg(kernel, 1, sizeof(cl_mem), (void *)& gvelnew);
    clSetKernelArg(kernel, 2, sizeof(cl_mem), (void *)& gposold);
    clSetKernelArg(kernel, 3, sizeof(cl_mem), (void *)& gvelold);

    cl_event eventB;
    ret = clEnqueueNDRangeKernel(command_queue, kernel, 1, NULL, &PARTICLES, &GROUPSIZE, 0, NULL, &eventB );
    clWaitForEvents(1, &eventB); // HOW ABOUT THE NULL RANGES HERE ? 

//    clFlush(command_queue); // Flushes the comm. queue?
}

// INITIALIZES PARTICLE DATA 
void cpu_initialize()
{
    posold = new float[4*PARTICLES];
    posnew = new float[4*PARTICLES];
    velold = new float[4*PARTICLES];
    velnew = new float[4*PARTICLES];

    srandom(time(NULL));  // seed the random number generator

    float zero_to_one_;
    float zero_to2pi_;
    float rad_;

    // Generate velocities & directions at random
    for (int i = 0; i < PARTICLES; i++) {
       zero_to_one_   = float(rand())/float(RAND_MAX);
       zero_to2pi_    = float(rand())/float(RAND_MAX) * float(M_PI)*2.0f;
       rad_ = sqrt(PARTICLES*2.0f* zero_to_one_); // why PART?

       posold[i*4]     = rad_*cos(zero_to2pi_) ;
       posold[i*4 + 1] = rad_*sin(zero_to2pi_); 
       posold[i*4 + 2] = 0.0f; // WHY?
       posold[i*4 + 3] = 1.0f;
    }
}

// ALLOCATES MEMORY
void allocate_memory()
{
   gposold = clCreateBuffer(context, CL_MEM_READ_WRITE, MEMSIZE , NULL, &ret);  
   gposnew = clCreateBuffer(context, CL_MEM_READ_WRITE, MEMSIZE , NULL, &ret);  
   gvelold = clCreateBuffer(context, CL_MEM_READ_WRITE, MEMSIZE , NULL, &ret);  
   gvelnew = clCreateBuffer(context, CL_MEM_READ_WRITE, MEMSIZE , NULL, &ret);  

   clEnqueueWriteBuffer(command_queue, gposold, CL_TRUE, 0, MEMSIZE, posold, 0, NULL, NULL);
   clEnqueueWriteBuffer(command_queue, gvelold, CL_TRUE, 0, MEMSIZE, velold, 0, NULL, NULL);
}

// LOADS THE KERNEL AND ATTACHES IT TO THE APP
void cl_attach_kernel()
{
    std::string source_   = loadFile("integrate_eom_kernel.cl");
    const char* c_source_ = source_.c_str();

    program = clCreateProgramWithSource(context, 1, &c_source_, NULL, &ret);
    ret     = clBuildProgram(program, 1, &device_id, NULL, NULL, NULL); 
    kernel  = clCreateKernel(program, "integrate_eom", &ret); 
    std::cout << (ret == CL_SUCCESS) << "\n";

}

void write_state(const char* filename)
{
    clEnqueueReadBuffer(command_queue, gposold, CL_TRUE, 0, MEMSIZE, posold, 0, NULL, NULL);
    clEnqueueReadBuffer(command_queue, gvelold, CL_TRUE, 0, MEMSIZE, velold, 0, NULL, NULL);
    
    FILE *fd = fopen(filename, "w");
    for (int i = 0; i < PARTICLES; i++) {
        fprintf(fd, "%e \t  %e \t", posold[i*4], posold[i*4+1]);
        fprintf(fd, "%e \t  %e \n", velold[i*4], velold[i*4+1]);
       // fprintf(fd, "%e %e %e %e", posold[i*4],posold[i*4+1],posold[i*4+2],posold[i*4+3]);
       // fprintf(fd, "%e %e %e %e", velold[i*4],velold[i*4+1],velold[i*4+2],velold[i*4+3]);
    }
    fclose(fd);
}

void put_electrons(int num, float* pos, float* vel)
{
    clEnqueueWriteBuffer(command_queue, gposold, CL_TRUE, 0, MEMSIZE, posold, 0, NULL, NULL);
    clEnqueueWriteBuffer(command_queue, gvelold, CL_TRUE, 0, MEMSIZE, velold, 0, NULL, NULL);
    clFinish(command_queue);

}

void get_electrons(int num, float* pos, float* vel)
{
    clEnqueueReadBuffer(command_queue, gposold, CL_TRUE, 0, MEMSIZE, posold, 0, NULL, NULL);
    clEnqueueReadBuffer(command_queue, gvelold, CL_TRUE, 0, MEMSIZE, velold, 0, NULL, NULL);
    clFlush(command_queue);
}

std::string loadFile(const char* filename)
{
    std::ifstream file(filename); 
    if(file)
    {
        std::ostringstream output;
        output << file.rdbuf();
        return output.str();
    }

}
